import com.google.common.collect.Lists
import com.mongodb.Mongo
import com.mongodb.WriteConcern
import net.persgroep.service.stats.common.constants.Action
import net.persgroep.service.stats.converter.ListMultimapReadConverter
import net.persgroep.service.stats.converter.ListMultimapWriteConverter
import net.persgroep.service.stats.converter.PeriodReadConverter
import net.persgroep.service.stats.converter.PeriodWriteConverter
import net.persgroep.service.stats.domain.TrackedActivity
import net.persgroep.service.stats.domain.TrackedActivitySample
import org.apache.commons.math3.distribution.ExponentialDistribution
import org.apache.commons.math3.distribution.IntegerDistribution
import org.apache.commons.math3.distribution.RealDistribution
import org.apache.commons.math3.distribution.UniformIntegerDistribution
import org.joda.time.DateTime
import org.joda.time.Duration
import org.springframework.data.mapping.context.MappingContext
import org.springframework.data.mongodb.MongoDbFactory
import org.springframework.data.mongodb.authentication.MongoDBUserCredentials
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.SimpleMongoDbFactory
import org.springframework.data.mongodb.core.convert.CustomConversions
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper
import org.springframework.data.mongodb.core.convert.MappingMongoConverter
import org.springframework.data.mongodb.core.mapping.MongoMappingContext

import static Constants.*

class Constants {
    static final int START_ID = 1
    static final int ORIGINS = 10
    static final int RESOURCES_PER_DAY = 100
    static final RealDistribution intervalPerOrigin = new ExponentialDistribution(Duration.standardDays(1).getMillis() / (RESOURCES_PER_DAY / ORIGINS))
    static final IntegerDistribution counts_10m = new UniformIntegerDistribution(0, 100)
    static final IntegerDistribution counts_1h = new UniformIntegerDistribution(0, 100)
    static final IntegerDistribution counts_1d = new UniformIntegerDistribution(0, 500)
    static final IntegerDistribution activityDays = new UniformIntegerDistribution(30, 90)
}

def cli = new CliBuilder(usage: 'groovy GenerateSamples')
cli.h(args: 1, argName: 'hostname', 'MongoDB hostname (required)')
cli.P(args: 1, argName: 'port', 'Port Number [27017]')
cli.d(args: 1, argName: 'database', 'Database [statsservice]')
cli.u(args: 1, argName: 'user', 'User []')
cli.p(args: 1, argName: 'password', 'Password []')
cli.a(args: 1, argName: 'authDb', 'Authentication Database []')

def options = cli.parse(args)
if (!options.h) {
    cli.usage()
    return;
}

def mongoHost = options.h
def mongoPort = options.P ?: '27017'
def mongoDb = options.d ?: 'statsservice'


def mongo = new Mongo(mongoHost, mongoPort as int)
def credentials = null
if (options.u || options.p || options.a) {
    credentials = new MongoDBUserCredentials(options.u, options.p, options.a)
}


MongoDbFactory mongoDbFactory = new SimpleMongoDbFactory(mongo, mongoDb, credentials)
MongoDbFactory mongoSampleDbFactory = new SimpleMongoDbFactory(mongo, mongoDb+"_sample", credentials)
MappingContext mappingContext = new MongoMappingContext()

MappingMongoConverter mappingMongoConverter = new MappingMongoConverter(mongoDbFactory, mappingContext)
mappingMongoConverter.setTypeMapper(new DefaultMongoTypeMapper(null))
mappingMongoConverter.afterPropertiesSet()
MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory, mappingMongoConverter)
MongoTemplate mongoSampleTemplate = new MongoTemplate(mongoSampleDbFactory, mappingMongoConverter)
mongoTemplate.setWriteConcern(WriteConcern.MAJORITY)
mongoSampleTemplate.setWriteConcern(WriteConcern.MAJORITY)
generateSamples(mongoTemplate, mongoSampleTemplate)

def generateSamples(MongoTemplate template, MongoTemplate sampleTemplate) {

    DateTime end = DateTime.now().plusDays(1)
    DateTime start = end.minusMonths(3)

    Thread[] threads = new Thread[ORIGINS]
    for (int originNr = 0; originNr < ORIGINS; ++originNr) {
        final int nr = originNr
        threads[originNr] = Thread.start({ generateSamples(start, end, template, sampleTemplate, nr) })
    }
    for (int originNr = 0; originNr < ORIGINS; ++originNr) {
        threads[originNr].join()
    }
}

def generateSamples(DateTime start, DateTime end, MongoTemplate template, MongoTemplate sampleTemplate, int originNr) {

    int resourceId = START_ID
    DateTime activityStart = start.plusMillis(intervalPerOrigin.sample() as int);
    while (activityStart.isBefore(end)) {
        def durationDays = activityDays.sample()
        DateTime activityEnd = activityStart.plusDays(durationDays)
        def origin = "origin_$originNr"
        int factor = 11 - Math.sqrt((resourceId % 100) + 1);
        for (def action in Action.values()) {
            def activity = new TrackedActivity(action.name(), origin, String.format("%09d", resourceId))
            activity.startTime = activityStart
            activity.endTime = activityEnd

            activity.addParameter("url", String.format("http://www.%s.be/article/%09d", origin, resourceId))

            int minutes = activityStart.getMinuteOfHour();
            def firstStart = activityStart.withSecondOfMinute(0).withMillisOfSecond(0).minusMinutes(minutes % 10).plusMinutes(10);

            List<TrackedActivitySample> samples = Lists.newArrayListWithCapacity(600)
            add10minuteSamples(samples, activity, firstStart, factor, end)
            addHourSamples(samples, activity, firstStart, factor, end)
            addDaySamples(samples, activity, firstStart, factor, end)
            sampleTemplate.insert(samples, TrackedActivitySample)

            template.save(activity)
        }
        if (resourceId % 50 == 1) {
            println("${DateTime.now()} $origin:${sprintf('%09d', resourceId)}: $activityStart / $activityEnd")
        }
        activityStart = activityStart.plusMillis(intervalPerOrigin.sample() as int)
        ++resourceId
    }

}

void add10minuteSamples(List<TrackedActivitySample> samples, TrackedActivity activity, DateTime firstStart, int factor, DateTime end) {
    for (int i in 0..287) {
        int count = counts_10m.sample() * factor / (i + 1)
        if (count > 0) {
            activity.total += count
            DateTime sampleEnd = firstStart.plusMinutes(i * 10 + 10)
            if (sampleEnd.isAfter(end)) {
                return
            }
            DateTime sampleStart = firstStart.plusMinutes(i * 10)
            samples.add(new TrackedActivitySample(
                    activity,
                    sampleStart,
                    count
            ))
        }
    }
}

void addHourSamples(List<TrackedActivitySample> samples, TrackedActivity activity, DateTime firstStart, int factor, DateTime end) {
    for (int i in 48..((14 * 24) - 1)) {
        int count = counts_1h.sample() * factor / i
        if (count > 0) {
            activity.total += count
            DateTime sampleStart = firstStart.plusHours(i)
            DateTime sampleEnd = firstStart.plusHours(i + 1)
            if (sampleEnd.isAfter(end)) {
                return
            }
            samples.add(new TrackedActivitySample(
                    activity,
                    sampleStart,
                    count
            ))
        }
    }
}


void addDaySamples(List<TrackedActivitySample> samples, TrackedActivity activity, DateTime firstStart, int factor, DateTime end) {
    for (int i in 14..90) {
        int count = counts_1d.sample() * factor / (i * i)
        if (count > 0) {
            activity.total += count
            DateTime sampleStart = firstStart.plusDays(i)
            DateTime sampleEnd = firstStart.plusDays(i + 1)
            if (sampleEnd.isAfter(end)) {
                return
            }
            samples.add(new TrackedActivitySample(
                    activity,
                    sampleStart,
                    count
            ))
        }
    }
}
