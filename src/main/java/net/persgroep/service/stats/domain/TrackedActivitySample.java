package net.persgroep.service.stats.domain;

import com.google.common.base.Objects;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * TracedActivitySample entity.
 *
 * @author gpanthe
 * @since 9/10/13
 */
@Document(collection = TrackedActivitySample.COLLECTION)
@CompoundIndexes({
        @CompoundIndex(name = "IDX_ACTION_ORIGIN_START_TIME", def = "{'" + TrackedActivitySample.ACTION + "' : 1, '" + TrackedActivitySample.ORIGIN + "' : 1, '" + TrackedActivitySample.TIME + "' : 1}"),
        @CompoundIndex(name = "IDX_ORIGIN_RESOURCE_ID", def = "{'" + TrackedActivitySample.ORIGIN + "' : 1, '" + TrackedActivitySample.RESOURCE_ID + "' : 1}")
})
public class TrackedActivitySample {

    public static final String COLLECTION = "tracked_activity_sample";

    public static final String TIME = "t";
    public static final String ACTION = "a";
    public static final String ORIGIN = "o";
    public static final String RESOURCE_ID = "r";
    public static final String PERIOD_COUNT = "p";

    @Id
    private String id;
    @Field(TIME)
    private final DateTime time;
    @Field(ACTION)
    private final String action;
    @Field(ORIGIN)
    private final String origin;
    @Field(RESOURCE_ID)
    private final String resourceId;
    @Field(PERIOD_COUNT)
    private int periodCount;

    public TrackedActivitySample(final TrackedActivity activity, final DateTime time, final int periodCount) {
        if (activity == null) {
            throw new IllegalArgumentException("activity should not be null");
        }
        this.action = activity.getAction();
        this.origin = activity.getOrigin();
        this.resourceId = activity.getResourceId();
        this.time = time;

        this.periodCount = periodCount;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public DateTime getTime() {
        return time;
    }

    public int getPeriodCount() {
        return periodCount;
    }

    public void setPeriodCount(final int periodCount) {
        this.periodCount = periodCount;
    }

    public String getAction() {
        return action;
    }

    public String getOrigin() {
        return origin;
    }

    public String getResourceId() {
        return resourceId;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final TrackedActivitySample that = (TrackedActivitySample) o;

        return Objects.equal(this.time, that.time)
                && Objects.equal(this.action, that.action)
                && Objects.equal(this.origin, that.origin)
                && Objects.equal(this.resourceId, that.resourceId)
                && Objects.equal(this.periodCount, that.periodCount);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(time, action, origin, resourceId, periodCount);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .addValue(time)
                .addValue(action)
                .addValue(origin)
                .addValue(resourceId)
                .addValue(periodCount)
                .toString();
    }
}
