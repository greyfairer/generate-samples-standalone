package net.persgroep.service.stats.domain;

import com.google.common.base.Objects;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;
import java.util.Map;

/**
 * A TrackedActivity is a combination of an action, an origin and a resourceId. The period a TrackedActivity is active is determined by the start and endDate
 */
@Document(collection = TrackedActivity.COLLECTION)
@CompoundIndexes({
        @CompoundIndex(name = "IDX_ACTION_ORIGIN_RESOURCE_ID", def = "{'" + TrackedActivity.ACTION + "' : 1, '" + TrackedActivity.ORIGIN + "' : 1, '" + TrackedActivity.RESOURCE_ID + "' : 1}", unique = true),
        @CompoundIndex(name = "IDX_ORIGIN_RESOURCE_ID", def = "{'" + TrackedActivity.ORIGIN + "' : 1, '" + TrackedActivity.RESOURCE_ID + "' : 1}")
})
public class TrackedActivity {

    public static final String COLLECTION = "tracked_activity";

    public static final String ACTION = "a";
    public static final String ORIGIN = "o";
    public static final String RESOURCE_ID = "r";
    public static final String START_TIME = "s";
    public static final String END_TIME = "e";
    public static final String LAST_AGGREGATED = "l";
    public static final String PARAMETERS = "p";
    public static final String VERSION = "v";
    public static final String TOTAL = "t";

    @Id
    private String id;

    @Field(ACTION)
    private String action;
    @Field(ORIGIN)
    private String origin;
    @Field(RESOURCE_ID)
    private String resourceId;

    @Field(START_TIME)
    private DateTime startTime;
    @Field(END_TIME)
    private DateTime endTime;

    @Field(LAST_AGGREGATED)
    private DateTime lastAggregated;

    @Field(PARAMETERS)
    private ListMultimap<String, String> parameters = ArrayListMultimap.create();

    @Field(VERSION)
    private int version;

    @Field(TOTAL)
    private long total;

    public TrackedActivity(final String action, final String origin, final String resourceId) {
        this.action = action;
        this.origin = origin;
        this.resourceId = resourceId;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(final String action) {
        this.action = action;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(final String origin) {
        this.origin = origin;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(final String resourceId) {
        this.resourceId = resourceId;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(final DateTime startTime) {
        this.startTime = startTime;
    }

    public DateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(final DateTime endTime) {
        this.endTime = endTime;
    }

    public ListMultimap<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(final ListMultimap<String, String> parameters) {
        this.parameters = parameters;
    }

    public void setParameters(final Map<String, List<String>> parameters) {
        final ListMultimap<String, String> parametersAsMultimap = ArrayListMultimap.create();
        for (final Map.Entry<String, List<String>> entry : parameters.entrySet()) {
            parametersAsMultimap.putAll(entry.getKey(), entry.getValue());
        }
        this.parameters = parametersAsMultimap;
    }

    public void addParameter(final String key, final String value) {
        if ("article_id".equals(key) && getParameters().containsKey("article_id") && !getParameters().get("article_id").isEmpty() && !getParameters().get("article_id").get(0).equals(value)) {
            throw new IllegalArgumentException("Article ids of all url parameters should be equal to one another");
        }
        parameters.put(key, value);
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(final long total) {
        this.total = total;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    public boolean isExpired() {
        if (endTime ==  null){
            return false;
        }
        return endTime.isBeforeNow();
    }

    public boolean isDailyScheduleRequired() {
        return false;
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final TrackedActivity that = (TrackedActivity) o;

        return Objects.equal(this.resourceId, that.resourceId)
                && Objects.equal(this.version, that.version)
                && Objects.equal(this.action, that.action)
                && Objects.equal(this.origin, that.origin)
                && Objects.equal(this.parameters, that.parameters);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(resourceId, version, action, origin);
    }


    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .addValue(action)
                .addValue(origin)
                .addValue(resourceId)
                .addValue(version)
                .addValue(parameters)
                .toString();
    }

    public DateTime getLastAggregated() {
        if (lastAggregated == null){
            return DateTime.now().minusYears(1000);
        }
        return lastAggregated;
    }

    public void setLastAggregated(DateTime lastAggregated) {
        this.lastAggregated = lastAggregated;
    }
}
